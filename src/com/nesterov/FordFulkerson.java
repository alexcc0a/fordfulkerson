package com.nesterov;

import java.util.*;

class FordFulkerson {
    private static final int V = 6; // Количество вершин графа.

    // Метод для поиска максимального потока с использованием алгоритма Форда-Фалкерсона.
    private boolean bfs(int rGraph[][], int s, int t, int parent[]) {
        boolean visited[] = new boolean[V]; // Массив для хранения информации о посещении каждой вершины.
        Arrays.fill(visited, false); // Заполнение массива false.

        Queue<Integer> queue = new LinkedList<>(); // Создание очереди для обхода графа в ширину.
        queue.add(s); // Добавление исходной вершины в очередь.
        visited[s] = true; // Пометить исходную вершину как посещенную.
        parent[s] = -1; // Установить родительскую вершину исходной вершины как -1.

        while (!queue.isEmpty()) { // Пока очередь не пуста.
            int u = queue.poll(); // Извлечь вершину из очереди.

            for (int v = 0; v < V; v++) { // Проход по всем вершинам графа.
                if (!visited[v] && rGraph[u][v] > 0) { // Если вершина не посещена и имеется ненасыщенное ребро.
                    queue.add(v); // Добавить вершину в очередь.
                    parent[v] = u; // Установить родительскую вершину для вершины v.
                    visited[v] = true; // Пометить вершину v как посещенную.
                }
            }
        }

        return visited[t]; // Вернуть результат посещения целевой вершины.
    }

    // Метод для нахождения максимального потока в графе.
    private int fordFulkerson(int graph[][], int s, int t) {
        int u, v;

        int rGraph[][] = new int[V][V]; // Создание остаточного графа.
        for (u = 0; u < V; u++) { // Копирование значений графа в остаточный граф.
            for (v = 0; v < V; v++) {
                rGraph[u][v] = graph[u][v]; // Копирование значения ребра из графа в остаточный граф.
            }
        }

        int parent[] = new int[V]; // Создание массива для хранения родительских вершин.

        int maxFlow = 0; // Максимальный поток.

        while (bfs(rGraph, s, t, parent)) { // Пока существует путь из истока в сток в остаточном графе.
            int pathFlow = Integer.MAX_VALUE; // Максимальный поток пути инициализируется максимально возможным значением.
            for (v = t; v != s; v = parent[v]) { // Проход по пути по родительским вершинам, начиная с целевой вершины.
                u = parent[v];
                pathFlow = Math.min(pathFlow, rGraph[u][v]); // Нахождение минимального пропускного ребра на пути.
            }

            for (v = t; v != s; v = parent[v]) { // Проход по пути по родительским вершинам, начиная с целевой вершины.
                u = parent[v];
                rGraph[u][v] -= pathFlow; // Уменьшение пропускных способностей ребер на пути.
                rGraph[v][u] += pathFlow; // Увеличение обратных ребер на пути.
            }

            maxFlow += pathFlow; // Добавление пропускного потока пути к общему пропуску.
        }

        return maxFlow; // Возвращение максимального потока.
    }

    public static void main(String[] args) {
        int graph[][] = {{0, 16, 13, 0, 0, 0},
                {0, 0, 10, 12, 0, 0},
                {0, 4, 0, 0, 14, 0},
                {0, 0, 9, 0, 0, 20},
                {0, 0, 0, 7, 0, 4},
                {0, 0, 0, 0, 0, 0}}; // Инициализация графа.

        FordFulkerson fordFulkerson = new FordFulkerson(); // Создание объекта FordFulkerson.

        int source = 0; // Исток.
        int sink = 5; // Сток.

        System.out.println("Максимальный поток: " + fordFulkerson.fordFulkerson(graph, source, sink));
    }
}
